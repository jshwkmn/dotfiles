#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ..='cd ..'
alias la='ls -a'
shopt -s autocd
alias mv='mv -i'
alias rm='rm -i'
alias c='clear'
export EDITOR=nvim
export PATH
export CLICOLOR=1
export HISTCONTROL=ignoreboth


PS1='[\h \W]\$ '
fish
