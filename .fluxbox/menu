[begin] (FluxBox)
    [submenu] (Development)
        [exec] (Geany) {geany}
        [exec] (IDLE [Python IDE]) {idle}
        [exec] (Meld) {meld}
        [exec] (Python) {xterm -e python}
        [exec] (Python [v2.7]) {xterm -e python2.7}
        [exec] (Tclsh8.6) {xterm -e tclsh8.6}
    [end]
    [submenu] (Editors)
        [exec] (AA Fire) {xterm -e aafire -driver slang}
        [exec] (Emacs) {xterm -e emacs}
        [exec] (Leafpad) {leafpad}
        [exec] (Nano) {xterm -e nano}
        [exec] (Neovim) {xterm -e nvim}
        [exec] (Vim) {xterm -e vim}
    [end]
    [submenu] (Games)
        [submenu] (Adventure)
            [exec] (Sclient) {sclient}
        [end]
        [submenu] (Arcade)
            [exec] (PacMan) {pacman}
        [end]
        [exec] (Terraria) {steam steam://rungameid/105600}
    [end]
    [submenu] (Graphics)
        [exec] (Viewnior) {viewnior}
    [end]
    [submenu] (Multimedia)
        [exec] (ALSA mixer) {xterm -e alsamixer}
        [exec] (Echomixer) {echomixer}
        [exec] (Envy24 Control) {envy24control}
        [exec] (HDAJackRetask) {hdajackretask}
        [exec] (HDSPConf) {hdspconf}
        [exec] (HDSPMixer) {hdspmixer}
        [exec] (Hwmixvolume) {hwmixvolume}
        [exec] (ncmpcpp) {xterm -e ncmpcpp}
        [exec] (PulseAudio Equalizer) {pulseaudio-equalizer-gtk}
        [exec] (PulseAudio Volume Control) {pavucontrol}
        [exec] (Rmedigicontrol) {rmedigicontrol}
    [end]
    [submenu] (Network)
        [exec] (Chromium) {chromium}
        [exec] (Discord) {discord}
        [exec] (FTP) {xterm -e ftp}
        [exec] (Mail Reader) {exo-open --launch MailReader}
        [exec] (Midori) {midori}
        [exec] (Steam [Runtime]) {steam-runtime}
        [exec] (Talk) {xterm -e talk}
        [exec] (Telnet) {xterm -e telnet}
        [exec] (TkVNC) {xterm -e tkvnc}
        [exec] (W3m) {xterm -e w3m}
        [exec] (w3m) {xterm -e w3m /usr/share/doc/w3m/MANUAL.html}
        [exec] (Web Browser) {exo-open --launch WebBrowser}
    [end]
    [submenu] (Office)
        [exec] (Atril Document Viewer) {atril}
        [exec] (PyRoom) {pyroom}
    [end]
    [submenu] (Science)
        [exec] (Calc) {xterm -e calc}
    [end]
    [submenu] (Shells)
        [exec] (Alacritty) {alacritty}
        [exec] (File Manager PCManFM) {pcmanfm}
        [exec] (GIT) {xterm -e git}
        [exec] (ranger) {xterm -e ranger}
        [exec] (Thunar File Manager) {thunar}
        [exec] (UXTerm) {uxterm}
        [exec] (Xfce Terminal) {xfce4-terminal}
        [exec] (Xterm) {xterm}
    [end]
    [submenu] (System)
        [submenu] (Settings)
            [submenu] (GNOME)
                [exec] (Advanced Network Configuration) {nm-connection-editor}
                [exec] (Bluetooth Manager) {blueman-manager}
            [end]
            [submenu] (Xfce)
                [exec] (Bluetooth Adapters) {blueman-adapters}
                [exec] (Settings Manager) {xfce4-settings-manager}
            [end]
            [exec] (Accessibility) {xfce4-accessibility-settings}
            [exec] (Appearance) {xfce4-appearance-settings}
            [exec] (Color Profiles) {xfce4-color-settings}
            [exec] (Customize Look and Feel) {lxappearance}
            [exec] (Default Applications) {xfce4-mime-settings}
            [exec] (Display) {xfce4-display-settings}
            [exec] (Keyboard) {xfce4-keyboard-settings}
            [exec] (Kvantum Manager) {kvantummanager}
            [exec] (Manage Printing) {xdg-open http://localhost:631/}
            [exec] (Mouse and Touchpad) {xfce4-mouse-settings}
            [exec] (nitrogen) {nitrogen}
            [exec] (Notifications) {xfce4-notifyd-config}
            [exec] (Openbox Configuration Manager) {obconf}
            [exec] (Power Manager) {xfce4-power-manager-settings}
            [exec] (Qt5 Settings) {qt5ct}
            [exec] (Settings Editor) {xfce4-settings-editor}
            [exec] (Xfce Terminal Settings) {xfce4-terminal --preferences}
            [exec] (YAD settings) {yad-settings}
        [end]
        [exec] (About Developer) {about.sh}
        [exec] (Disk Usage Analyzer) {baobab}
        [exec] (fish) {xterm -e fish}
        [exec] (Getting Started) {get_started.sh}
        [exec] (GParted) {gparted}
        [exec] (Hardware Locality lstopo) {lstopo}
        [exec] (Htop) {xterm -e htop}
        [exec] (Pstree) {xterm -e pstree.x11}
        [exec] (pstree) {xterm -e /usr/bin/pstree.x11}
        [exec] (pstree) {xterm -e pstree}
        [exec] (Timeshift) {timeshift-launcher}
        [exec] (Top) {xterm -e top}
        [exec] (Xkill) {xkill}
    [end]
    [submenu] (Utilities)
        [exec] (Bc) {xterm -e bc}
        [exec] (Console Matrix [Text]) {xterm -e cmatrix}
        [exec] (Console Matrix [Virtual Console]) {xterm -e cmatrix -l}
        [exec] (Console Matrix [X11]) {cmatrix -x}
        [exec] (Dc) {xterm -e dc}
        [exec] (File Manager) {exo-open --launch FileManager}
        [exec] (Info) {xterm -e info}
        [exec] (Plank) {plank}
        [exec] (Terminal Emulator) {exo-open --launch TerminalEmulator}
        [exec] (Xarchiver) {xarchiver}
    [end]
    [separator]
    [submenu] (FluxBox)
        [workspaces] (Workspaces)
        [submenu] (Styles)
            [stylesdir] (/usr/share/fluxbox/styles)
            [stylesdir] (~/.fluxbox/styles)
        [end]
        [config] (Configure)
        [reconfig] (Reconfig)
        [restart] (Restart)
        [separator]
        [exit] (Exit)
    [end]
[end]
